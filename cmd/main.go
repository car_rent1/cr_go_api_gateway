package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/car_rent1/cr_go_api_gateway/api"
	"gitlab.com/car_rent1/cr_go_api_gateway/api/handlers"
	"gitlab.com/car_rent1/cr_go_api_gateway/config"
	"gitlab.com/car_rent1/cr_go_api_gateway/pkg/logger"
	"gitlab.com/car_rent1/cr_go_api_gateway/services"
)

func main() {
	cfg := config.Load()

	loggerlevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerlevel = logger.LevelDebug
	case config.TestMode:
		loggerlevel = logger.LevelDebug
	default:
		loggerlevel = logger.LevelInfo
	}
	log := logger.NewLogger(cfg.ServiceName, loggerlevel)
	defer logger.Cleanup(log)

	fmt.Printf("config: %+v\n", cfg)

	grpcSrvc, err := services.NewGrpcClients(cfg)
	if err != nil {
		panic(err)
	}

	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())

	h := handlers.NewHandler(cfg, log, grpcSrvc)

	api.SetUpAPI(r, h, cfg)

	fmt.Println("Start api gateway...")

	err = r.Run(cfg.HTTPPort)
	if err != nil {
		return
	}
}
