package services

import (
	"gitlab.com/car_rent1/cr_go_api_gateway/config"
	"gitlab.com/car_rent1/cr_go_api_gateway/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_api_gateway/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_api_gateway/genproto/user_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	InvestorService() user_service.InvestorServiceClient
	CarService() car_service.CarServiceClient
	OrderService() order_service.OrderServiceClient
	GiveCarService() order_service.GiveCarServiceClient
	ReceiveCarService() order_service.ReceiveCarServiceClient
	OrderPaymentService() order_service.OrderPaymentServiceClient
	TarifService() order_service.TarifServiceClient
	BrandService() car_service.BrandServiceClient
	ModelService() car_service.ModelServiceClient
	ClientService() user_service.ClientServiceClient
	MechanicService() user_service.MechanikServiceClient
	BranchService() user_service.BranchServiceClient
}

type grpcClients struct {
	userService  user_service.InvestorServiceClient
	carService   car_service.CarServiceClient
	orderService order_service.OrderServiceClient
	giveCar      order_service.GiveCarServiceClient
	receiveCar   order_service.ReceiveCarServiceClient
	orderPayment order_service.OrderPaymentServiceClient
	tarif        order_service.TarifServiceClient
	brand        car_service.BrandServiceClient
	model        car_service.ModelServiceClient
	client       user_service.ClientServiceClient
	mechanic     user_service.MechanikServiceClient
	branch       user_service.BranchServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// connection to Order Microservice

	connInvestorService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	connCarService, err := grpc.Dial(
		cfg.CarServiceHost+cfg.CarGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	connOrderService, err := grpc.Dial(
		cfg.OrderServiceHost+cfg.OrderGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		userService:  user_service.NewInvestorServiceClient(connInvestorService),
		carService:   car_service.NewCarServiceClient(connCarService),
		orderService: order_service.NewOrderServiceClient(connOrderService),
		giveCar:      order_service.NewGiveCarServiceClient(connOrderService),
		receiveCar:   order_service.NewReceiveCarServiceClient(connOrderService),
		orderPayment: order_service.NewOrderPaymentServiceClient(connOrderService),
		tarif:        order_service.NewTarifServiceClient(connOrderService),
		brand:        car_service.NewBrandServiceClient(connCarService),
		model:        car_service.NewModelServiceClient(connCarService),
		client:       user_service.NewClientServiceClient(connInvestorService),
		branch:       user_service.NewBranchServiceClient(connInvestorService),
		mechanic:     user_service.NewMechanikServiceClient(connInvestorService),
	}, nil
}

func (g *grpcClients) InvestorService() user_service.InvestorServiceClient {
	return g.userService
}
func (g *grpcClients) CarService() car_service.CarServiceClient {
	return g.carService
}
func (g *grpcClients) OrderService() order_service.OrderServiceClient {
	return g.orderService
}
func (g *grpcClients) GiveCarService() order_service.GiveCarServiceClient {
	return g.giveCar
}

func (g *grpcClients) ReceiveCarService() order_service.ReceiveCarServiceClient {
	return g.receiveCar
}

func (g *grpcClients) OrderPaymentService() order_service.OrderPaymentServiceClient {
	return g.orderPayment
}
func (g *grpcClients) TarifService() order_service.TarifServiceClient {
	return g.tarif
}
func (g *grpcClients) BrandService() car_service.BrandServiceClient {
	return g.brand
}
func (g *grpcClients) ModelService() car_service.ModelServiceClient {
	return g.model
}
func (g *grpcClients) ClientService() user_service.ClientServiceClient {
	return g.client
}
func (g *grpcClients) MechanicService() user_service.MechanikServiceClient {
	return g.mechanic
}

func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branch
}
