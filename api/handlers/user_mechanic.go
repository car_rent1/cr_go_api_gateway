package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rent1/cr_go_api_gateway/genproto/user_service"
)

// CreateMechanic godoc
// @ID create_mechanic
// @Router /mechanic [POST]
// @Summary  Create Mechanic
// @Description Create Mechanic
// @Tags Mechanic
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body user_service.CreateMechanic true "CreateMechanicRequestBody"
// @Success 200 {object} http.Response{data=user_service.Mechanic} "GetMechanicBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateMechanic(c *gin.Context) {
	var mechanic user_service.CreateMechanic

	err := c.ShouldBindJSON(&mechanic)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.MechanicService().Create(c, &mechanic)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// GetMechanicByID godoc
// @ID get_mechanic_by_id
// @Router /mechanic/{id} [GET]
// @Summary Get Mechanic By ID
// @Description Get Mechanic By ID
// @Tags Mechanic
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.Mechanic} "MechanicBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetMechanicById(c *gin.Context) {

	userId := c.Param("id")

	resp, err := h.services.MechanicService().GetByID(c, &user_service.MechanicPrimaryKey{Id: userId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetMechanicList godoc
// @ID get_mechanic_list
// @Router /mechanic [GET]
// @Summary Get Mechanic s List
// @Description  Get Mechanic s List
// @Tags Mechanic
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchName query string false "searchName"
// @Param searchPhone query string false "searchPhone"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=user_service.GetListMechanicResponse} "GetAllMechanicResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetMechanicList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.MechanicService().GetList(
		context.Background(),
		&user_service.GetListMechanicRequest{
			Limit:       int64(limit),
			Offset:      int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateMechanic godoc
// @ID update_mechanic
// @Router /mechanic/{id} [PUT]
// @Summary Update Mechanic
// @Description Update Mechanic
// @Tags Mechanic
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateMechanic true "UpdateMechanicRequestBody"
// @Success 200 {object} http.Response{data=user_service.Mechanic} "Mechanic data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateMechanic(c *gin.Context) {
	var user user_service.UpdateMechanic

	user.Id = c.Param("id")

	err := c.ShouldBindJSON(&user)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.MechanicService().Update(
		c.Request.Context(),
		&user,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// DeleteUser godoc
// @ID delete_user
// @Router /user/{id} [DELETE]
// @Summary Delete User
// @Description Delete User
// @Tags User
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Mechanic data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteMechanic(c *gin.Context) {

	userId := c.Param("id")

	resp, err := h.services.MechanicService().Delete(
		c.Request.Context(),
		&user_service.MechanicPrimaryKey{Id: userId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
