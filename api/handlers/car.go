package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rent1/cr_go_api_gateway/genproto/car_service"
)

// CreateCar godoc
// @ID create_car
// @Router /car [POST]
// @Summary  Create Car
// @Description Create Car
// @Tags Car
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body car_service.CreateCar true "CreateCarRequestBody"
// @Success 200 {object} http.Response{data=car_service.Car} "GetCarBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateCar(c *gin.Context) {
	var car car_service.CreateCar

	err := c.ShouldBindJSON(&car)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.CarService().Create(c, &car)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}



	c.JSON(http.StatusCreated, resp)

}

// GetCarByID godoc
// @ID get_car_by_id
// @Router /car/{id} [GET]
// @Summary Get Car By ID
// @Description Get Car By ID
// @Tags Car
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=car_service.Car} "InvestorBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCarById(c *gin.Context) {

	userId := c.Param("id")

	resp, err := h.services.CarService().GetByID(c, &car_service.CarPrimaryKey{Id: userId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetCarList godoc
// @ID get_car
// @Router /car [GET]
// @Summary Get Car s List
// @Description  Get Car s List
// @Tags Car
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchBrand query string false "searchbrand"
// @Param searchModel query string false "searchModel"
// @Param searchStateNumber query string false "searchStateNumber"
// @Param searchInvestor query string false "searchInvestor"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=car_service.GetListCarResponse} "GetAllCarResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetCarList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.CarService().GetList(
		context.Background(),
		&car_service.GetListCarRequest{
			Limit:             int64(limit),
			Offset:            int64(offset),
			SearchBrand:       c.Query("searchBrand"),
			SearchModel:       c.Query("searchModel"),
			SearchStateNumber: c.Query("searchStateNumber"),
			SearchInvestor:    c.Query("searchInvestor"),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateCar godoc
// @ID update_car
// @Router /car/{id} [PUT]
// @Summary Update Car
// @Description Update Car
// @Tags Car
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body car_service.UpdateCar true "UpdateCarRequestBody"
// @Success 200 {object} http.Response{data=car_service.Car} "Car data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateCar(c *gin.Context) {
	var car car_service.UpdateCar

	car.Id = c.Param("id")

	err := c.ShouldBindJSON(&car)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.CarService().Update(
		c.Request.Context(),
		&car,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// DeleteCar godoc
// @ID delete_car
// @Router /car/{id} [DELETE]
// @Summary Delete Car
// @Description Delete Car
// @Tags Car
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Car data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteCar(c *gin.Context) {

	userId := c.Param("id")

	resp, err := h.services.CarService().Delete(
		c.Request.Context(),
		&car_service.CarPrimaryKey{Id: userId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
