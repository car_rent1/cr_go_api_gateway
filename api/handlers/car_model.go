package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/car_rent1/cr_go_api_gateway/genproto/car_service"
)

// CreateModel godoc
// @ID create_model
// @Router /model [POST]
// @Summary  Create Model
// @Description Create Model
// @Tags Model
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body car_service.CreateModel true "CreateModelRequestBody"
// @Success 200 {object} http.Response{data=car_service.Model} "GetModelBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateModel(c *gin.Context) {
	var model car_service.CreateModel

	err := c.ShouldBindJSON(&model)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ModelService().Create(c, &model)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, resp)

}

// GetModelByID godoc
// @ID get_model_by_id
// @Router /model/{id} [GET]
// @Summary Get Model By ID
// @Description Get Model By ID
// @Tags Model
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=car_service.Model} "InvestorBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetModelById(c *gin.Context) {

	userId := c.Param("id")

	resp, err := h.services.ModelService().GetByID(c, &car_service.ModelPrimaryKey{Id: userId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetModelList godoc
// @ID get_model
// @Router /model [GET]
// @Summary Get Model s List
// @Description  Get Model s List
// @Tags Model
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchModel query string false "searchmodel"
// @Param searchModel query string false "searchModel"
// @Param searchStateNumber query string false "searchStateNumber"
// @Param searchInvestor query string false "searchInvestor"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=car_service.GetListModelResponse} "GetAllModelResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetModelList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ModelService().GetList(
		context.Background(),
		&car_service.GetListModelRequest{
			Limit:             int64(limit),
			Offset:            int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateModel godoc
// @ID update_model
// @Router /model/{id} [PUT]
// @Summary Update Model
// @Description Update Model
// @Tags Model
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body car_service.UpdateModel true "UpdateModelRequestBody"
// @Success 200 {object} http.Response{data=car_service.Model} "Model data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateModel(c *gin.Context) {
	var model car_service.UpdateModel

	model.Id = c.Param("id")

	err := c.ShouldBindJSON(&model)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ModelService().Update(
		c.Request.Context(),
		&model,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// DeleteModel godoc
// @ID delete_model
// @Router /model/{id} [DELETE]
// @Summary Delete Model
// @Description Delete Model
// @Tags Model
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Model data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteModel(c *gin.Context) {

	userId := c.Param("id")

	resp, err := h.services.ModelService().Delete(
		c.Request.Context(),
		&car_service.ModelPrimaryKey{Id: userId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
