package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/car_rent1/cr_go_api_gateway/genproto/order_service"
)

// CreateOrderPayment godoc
// @ID create_order_payment
// @Router /order_payment [POST]
// @Summary  Create OrderPayment
// @Description Create OrderPayment
// @Tags OrderPayment
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body order_payment_service.CreateOrderPayment true "CreateOrderPaymentRequestBody"
// @Success 200 {object} http.Response{data=order_payment_service.OrderPayment} "GetOrderPaymentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateOrderPayment(c *gin.Context) {
	var receiveCar order_service.CreateOrderPayment

	err := c.ShouldBindJSON(&receiveCar)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.OrderPaymentService().Create(c, &receiveCar)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// GetOrderPaymentByID godoc
// @ID get_order_payment_by_id
// @Router /order_payment/{id} [GET]
// @Summary Get OrderPayment By ID
// @Description Get OrderPayment By ID
// @Tags OrderPayment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=order_payment_service.OrderPayment} "OrderPaymentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetOrderPaymentById(c *gin.Context) {

	order_paymentId := c.Param("id")

	resp, err := h.services.OrderPaymentService().GetByID(c, &order_service.OrderPaymentPrimaryKey{Id: order_paymentId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetOrderPaymentList godoc
// @ID get_order_payment_list
// @Router /order_payment [GET]
// @Summary Get OrderPayment s List
// @Description  Get OrderPayment s List
// @Tags OrderPayment
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchTarif query string false "searchTarif"
// @Param searchDate query string false "searchDate"
// @Param searchOrderPaymentNumber query string false "searchOrderPaymentNumber"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=order_payment_service.GetListOrderPaymentResponse} "GetAllOrderPaymentResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetOrderPaymentList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.OrderPaymentService().GetList(
		context.Background(),
		&order_service.GetListOrderPaymentRequest{
			Limit:               int64(limit),
			Offset:              int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateOrderPayment godoc
// @ID update_order_payment
// @Router /order_payment/{id} [PUT]
// @Summary Update OrderPayment
// @Description Update OrderPayment
// @Tags OrderPayment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body order_payment_service.UpdateOrderPayment true "UpdateOrderPaymentRequestBody"
// @Success 200 {object} http.Response{data=order_payment_service.OrderPayment} "OrderPayment data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateOrderPayment(c *gin.Context) {
	var order_payment order_service.UpdateOrderPayment

	order_payment.Id = c.Param("id")

	err := c.ShouldBindJSON(&order_payment)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.OrderPaymentService().Update(
		c.Request.Context(),
		&order_payment,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// Deleteorder_payment godoc
// @ID delete_order_payment
// @Router /order_payment/{id} [DELETE]
// @Summary Delete OrderPayment
// @Description Delete OrderPayment
// @Tags OrderPayment
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "OrderPayment data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteOrderPayment(c *gin.Context) {

	order_paymentId := c.Param("id")

	resp, err := h.services.OrderPaymentService().Delete(
		c.Request.Context(),
		&order_service.OrderPaymentPrimaryKey{Id: order_paymentId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
