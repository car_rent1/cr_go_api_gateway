package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rent1/cr_go_api_gateway/genproto/user_service"
)

// CreateInvestor godoc
// @ID create_investor
// @Router /investor [POST]
// @Summary  Create Investor
// @Description Create Investor
// @Tags Investor
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body user_service.CreateInvestor true "CreateInvestorRequestBody"
// @Success 200 {object} http.Response{data=user_service.Investor} "GetInvestorBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateInvestor(c *gin.Context) {
	var investor user_service.CreateInvestor

	err := c.ShouldBindJSON(&investor)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.InvestorService().Create(c, &investor)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// GetInvestorByID godoc
// @ID get_investor_by_id
// @Router /investor/{id} [GET]
// @Summary Get Investor By ID
// @Description Get Investor By ID
// @Tags Investor
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.Investor} "InvestorBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetInvestorById(c *gin.Context) {

	userId := c.Param("id")

	resp, err := h.services.InvestorService().GetByID(c, &user_service.InvestorPrimaryKey{Id: userId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetInvestorList godoc
// @ID get_investor_list
// @Router /investor [GET]
// @Summary Get Investor s List
// @Description  Get Investor s List
// @Tags Investor
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchName query string false "searchName"
// @Param searchPhone query string false "searchPhone"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=user_service.GetListInvestorResponse} "GetAllInvestorResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetInvestorList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.InvestorService().GetList(
		context.Background(),
		&user_service.GetListInvestorRequest{
			Limit:       int64(limit),
			Offset:      int64(offset),
			SearchName:  c.Query("searchName"),
			SearchPhone: c.Query("searchPhone"),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateInvestor godoc
// @ID update_investor
// @Router /investor/{id} [PUT]
// @Summary Update Investor
// @Description Update Investor
// @Tags Investor
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateInvestor true "UpdateInvestorRequestBody"
// @Success 200 {object} http.Response{data=user_service.Investor} "Investor data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateInvestor(c *gin.Context) {
	var user user_service.UpdateInvestor

	user.Id = c.Param("id")

	err := c.ShouldBindJSON(&user)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.InvestorService().Update(
		c.Request.Context(),
		&user,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// DeleteUser godoc
// @ID delete_user
// @Router /user/{id} [DELETE]
// @Summary Delete User
// @Description Delete User
// @Tags User
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Investor data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteInvestor(c *gin.Context) {

	userId := c.Param("id")

	resp, err := h.services.InvestorService().Delete(
		c.Request.Context(),
		&user_service.InvestorPrimaryKey{Id: userId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
