package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/car_rent1/cr_go_api_gateway/genproto/user_service"
)

// CreateClient godoc
// @ID create_client
// @Router /client [POST]
// @Summary  Create Client
// @Description Create Client
// @Tags Client
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body user_service.CreateClient true "CreateClientRequestBody"
// @Success 200 {object} http.Response{data=user_service.Client} "GetClientBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateClient(c *gin.Context) {
	var client user_service.CreateClient

	err := c.ShouldBindJSON(&client)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ClientService().Create(c, &client)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// GetClientByID godoc
// @ID get_client_by_id
// @Router /client/{id} [GET]
// @Summary Get Client By ID
// @Description Get Client By ID
// @Tags Client
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.Client} "ClientBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetClientById(c *gin.Context) {

	userId := c.Param("id")

	resp, err := h.services.ClientService().GetByID(c, &user_service.ClientPrimaryKey{Id: userId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetClientList godoc
// @ID get_client_list
// @Router /client [GET]
// @Summary Get Client s List
// @Description  Get Client s List
// @Tags Client
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchName query string false "searchName"
// @Param searchPhone query string false "searchPhone"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=user_service.GetListClientResponse} "GetAllClientResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetClientList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ClientService().GetList(
		context.Background(),
		&user_service.GetListClientRequest{
			Limit:       int64(limit),
			Offset:      int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateClient godoc
// @ID update_client
// @Router /client/{id} [PUT]
// @Summary Update Client
// @Description Update Client
// @Tags Client
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateClient true "UpdateClientRequestBody"
// @Success 200 {object} http.Response{data=user_service.Client} "Client data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateClient(c *gin.Context) {
	var user user_service.UpdateClient

	user.Id = c.Param("id")

	err := c.ShouldBindJSON(&user)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ClientService().Update(
		c.Request.Context(),
		&user,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// DeleteUser godoc
// @ID delete_user
// @Router /user/{id} [DELETE]
// @Summary Delete User
// @Description Delete User
// @Tags User
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Client data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteClient(c *gin.Context) {

	userId := c.Param("id")

	resp, err := h.services.ClientService().Delete(
		c.Request.Context(),
		&user_service.ClientPrimaryKey{Id: userId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
