package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/car_rent1/cr_go_api_gateway/genproto/car_service"
)

// CreateBrand godoc
// @ID create_brand
// @Router /brand [POST]
// @Summary  Create Brand
// @Description Create Brand
// @Tags Brand
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body car_service.CreateBrand true "CreateBrandRequestBody"
// @Success 200 {object} http.Response{data=car_service.Brand} "GetBrandBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateBrand(c *gin.Context) {
	var brand car_service.CreateBrand

	err := c.ShouldBindJSON(&brand)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.BrandService().Create(c, &brand)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, resp)

}

// GetBrandByID godoc
// @ID get_brand_by_id
// @Router /brand/{id} [GET]
// @Summary Get Brand By ID
// @Description Get Brand By ID
// @Tags Brand
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=car_service.Brand} "InvestorBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBrandById(c *gin.Context) {

	userId := c.Param("id")

	resp, err := h.services.BrandService().GetByID(c, &car_service.BrandPrimaryKey{Id: userId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetBrandList godoc
// @ID get_brand
// @Router /brand [GET]
// @Summary Get Brand s List
// @Description  Get Brand s List
// @Tags Brand
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchBrand query string false "searchbrand"
// @Param searchModel query string false "searchModel"
// @Param searchStateNumber query string false "searchStateNumber"
// @Param searchInvestor query string false "searchInvestor"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=car_service.GetListBrandResponse} "GetAllBrandResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBrandList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.BrandService().GetList(
		context.Background(),
		&car_service.GetListBrandRequest{
			Limit:             int64(limit),
			Offset:            int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateBrand godoc
// @ID update_brand
// @Router /brand/{id} [PUT]
// @Summary Update Brand
// @Description Update Brand
// @Tags Brand
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body car_service.UpdateBrand true "UpdateBrandRequestBody"
// @Success 200 {object} http.Response{data=car_service.Brand} "Brand data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateBrand(c *gin.Context) {
	var brand car_service.UpdateBrand

	brand.Id = c.Param("id")

	err := c.ShouldBindJSON(&brand)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.BrandService().Update(
		c.Request.Context(),
		&brand,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// DeleteBrand godoc
// @ID delete_brand
// @Router /brand/{id} [DELETE]
// @Summary Delete Brand
// @Description Delete Brand
// @Tags Brand
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Brand data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteBrand(c *gin.Context) {

	userId := c.Param("id")

	resp, err := h.services.BrandService().Delete(
		c.Request.Context(),
		&car_service.BrandPrimaryKey{Id: userId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
