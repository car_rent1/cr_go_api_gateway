package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/car_rent1/cr_go_api_gateway/genproto/order_service"
)

// CreateReceiveCar godoc
// @ID create_receive_car
// @Router /receive_car [POST]
// @Summary  Create ReceiveCar
// @Description Create ReceiveCar
// @Tags ReceiveCar
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body receive_car_service.CreateReceiveCar true "CreateReceiveCarRequestBody"
// @Success 200 {object} http.Response{data=receive_car_service.ReceiveCar} "GetReceiveCarBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateReceiveCar(c *gin.Context) {
	var receiveCar order_service.CreateReceiveCar

	err := c.ShouldBindJSON(&receiveCar)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ReceiveCarService().Create(c, &receiveCar)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// GetReceiveCarByID godoc
// @ID get_receive_car_by_id
// @Router /receive_car/{id} [GET]
// @Summary Get ReceiveCar By ID
// @Description Get ReceiveCar By ID
// @Tags ReceiveCar
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=receive_car_service.ReceiveCar} "ReceiveCarBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetReceiveCarById(c *gin.Context) {

	receive_carId := c.Param("id")

	resp, err := h.services.ReceiveCarService().GetByID(c, &order_service.ReceiveCarPrimaryKey{Id: receive_carId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetReceiveCarList godoc
// @ID get_receive_car_list
// @Router /receive_car [GET]
// @Summary Get ReceiveCar s List
// @Description  Get ReceiveCar s List
// @Tags ReceiveCar
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchTarif query string false "searchTarif"
// @Param searchDate query string false "searchDate"
// @Param searchReceiveCarNumber query string false "searchReceiveCarNumber"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=receive_car_service.GetListReceiveCarResponse} "GetAllReceiveCarResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetReceiveCarList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ReceiveCarService().GetList(
		context.Background(),
		&order_service.GetListReceiveCarRequest{
			Limit:               int64(limit),
			Offset:              int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateReceiveCar godoc
// @ID update_receive_car
// @Router /receive_car/{id} [PUT]
// @Summary Update ReceiveCar
// @Description Update ReceiveCar
// @Tags ReceiveCar
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body receive_car_service.UpdateReceiveCar true "UpdateReceiveCarRequestBody"
// @Success 200 {object} http.Response{data=receive_car_service.ReceiveCar} "ReceiveCar data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateReceiveCar(c *gin.Context) {
	var receive_car order_service.UpdateReceiveCar

	receive_car.Id = c.Param("id")

	err := c.ShouldBindJSON(&receive_car)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ReceiveCarService().Update(
		c.Request.Context(),
		&receive_car,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// Deletereceive_car godoc
// @ID delete_receive_car
// @Router /receive_car/{id} [DELETE]
// @Summary Delete ReceiveCar
// @Description Delete ReceiveCar
// @Tags ReceiveCar
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "ReceiveCar data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteReceiveCar(c *gin.Context) {

	receive_carId := c.Param("id")

	resp, err := h.services.ReceiveCarService().Delete(
		c.Request.Context(),
		&order_service.ReceiveCarPrimaryKey{Id: receive_carId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
