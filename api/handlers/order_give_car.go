package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/car_rent1/cr_go_api_gateway/genproto/order_service"
)

// CreateGiveCar godoc
// @ID create_give_car
// @Router /give_car [POST]
// @Summary  Create GiveCar
// @Description Create GiveCar
// @Tags GiveCar
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body give_car_service.CreateGiveCar true "CreateGiveCarRequestBody"
// @Success 200 {object} http.Response{data=give_car_service.GiveCar} "GetGiveCarBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateGiveCar(c *gin.Context) {
	var giveCar order_service.CreateGiveCar

	err := c.ShouldBindJSON(&giveCar)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.GiveCarService().Create(c, &giveCar)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// GetGiveCarByID godoc
// @ID get_give_car_by_id
// @Router /give_car/{id} [GET]
// @Summary Get GiveCar By ID
// @Description Get GiveCar By ID
// @Tags GiveCar
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=give_car_service.GiveCar} "GiveCarBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetGiveCarById(c *gin.Context) {

	give_carId := c.Param("id")

	resp, err := h.services.GiveCarService().GetByID(c, &order_service.GiveCarPrimaryKey{Id: give_carId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetGiveCarList godoc
// @ID get_give_car_list
// @Router /give_car [GET]
// @Summary Get GiveCar s List
// @Description  Get GiveCar s List
// @Tags GiveCar
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchTarif query string false "searchTarif"
// @Param searchDate query string false "searchDate"
// @Param searchGiveCarNumber query string false "searchGiveCarNumber"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=give_car_service.GetListGiveCarResponse} "GetAllGiveCarResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetGiveCarList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.GiveCarService().GetList(
		context.Background(),
		&order_service.GetListGiveCarRequest{
			Limit:               int64(limit),
			Offset:              int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateGiveCar godoc
// @ID update_give_car
// @Router /give_car/{id} [PUT]
// @Summary Update GiveCar
// @Description Update GiveCar
// @Tags GiveCar
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body give_car_service.UpdateGiveCar true "UpdateGiveCarRequestBody"
// @Success 200 {object} http.Response{data=give_car_service.GiveCar} "GiveCar data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateGiveCar(c *gin.Context) {
	var give_car order_service.UpdateGiveCar

	give_car.Id = c.Param("id")

	err := c.ShouldBindJSON(&give_car)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.GiveCarService().Update(
		c.Request.Context(),
		&give_car,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// Deletegive_car godoc
// @ID delete_give_car
// @Router /give_car/{id} [DELETE]
// @Summary Delete GiveCar
// @Description Delete GiveCar
// @Tags GiveCar
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "GiveCar data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteGiveCar(c *gin.Context) {

	give_carId := c.Param("id")

	resp, err := h.services.GiveCarService().Delete(
		c.Request.Context(),
		&order_service.GiveCarPrimaryKey{Id: give_carId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
