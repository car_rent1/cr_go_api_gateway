package handlers

import (
	"encoding/json"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/gogo/protobuf/jsonpb"
	"gitlab.com/car_rent1/cr_go_api_gateway/api/http"
	"gitlab.com/car_rent1/cr_go_api_gateway/config"
	"gitlab.com/car_rent1/cr_go_api_gateway/pkg/logger"
	"gitlab.com/car_rent1/cr_go_api_gateway/services"
	"google.golang.org/protobuf/runtime/protoiface"
)

type Handler struct {
	cfg      config.Config
	log      logger.LoggerI
	services services.ServiceManagerI
}

func NewHandler(cfg config.Config, log logger.LoggerI, srvc services.ServiceManagerI) *Handler {
	return &Handler{
		cfg:      cfg,
		log:      log,
		services: srvc,
	}
}
func (h *Handler) handleResponse(c *gin.Context, status http.Status, data interface{}) {
	switch code := status.Code; {
	case code < 300:
		h.log.Info(
			"response",
			logger.Int("code", status.Code),
			logger.String("status", status.Status),
			logger.Any("description", status.Description),
			// logger.Any("data", data),
		)
	case code < 400:
		h.log.Warn(
			"response",
			logger.Int("code", status.Code),
			logger.String("status", status.Status),
			logger.Any("description", status.Description),
			logger.Any("data", data),
		)
	default:
		h.log.Error(
			"response",
			logger.Int("code", status.Code),
			logger.String("status", status.Status),
			logger.Any("description", status.Description),
			logger.Any("data", data),
		)
	}

	c.JSON(status.Code, data)
}

func ProtoToStruct(s interface{}, p protoiface.MessageV1) error {
	var jm jsonpb.Marshaler

	jm.EmitDefaults = true
	jm.OrigName = true

	ms, err := jm.MarshalToString(p)

	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(ms), &s)

	return err
}

func (h *Handler) getOffsetParam(c *gin.Context) (offset int, err error) {
	if h.cfg.DefaultOffset != "" {
		offsetStr := c.DefaultQuery("offset", h.cfg.DefaultOffset)
		return strconv.Atoi(offsetStr)
	}

	offsetStr := c.DefaultQuery("offset", "0")
	return strconv.Atoi(offsetStr)
}

func (h *Handler) getLimitParam(c *gin.Context) (offset int, err error) {
	if h.cfg.DefaultLimit != "" {
		limitStr := c.DefaultQuery("limit", h.cfg.DefaultLimit)
		return strconv.Atoi(limitStr)
	}
	limitStr := c.DefaultQuery("limit", "10")
	return strconv.Atoi(limitStr)
}

func (h *Handler) handleErrorResponse(c *gin.Context, code int, message string, err interface{}) {
	h.log.Error(message, logger.Int("code", code), logger.Any("error", err))
	c.JSON(code, ResponseModel{
		Code:    code,
		Message: message,
		Error:   err,
	})
}

func (h *Handler) handleSuccessResponse(c *gin.Context, code int, message string, data interface{}) {
	c.JSON(code, ResponseModel{
		Code:    code,
		Message: message,
		Data:    data,
	})
}
