package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/car_rent1/cr_go_api_gateway/genproto/order_service"
)

// CreateTarif godoc
// @ID create_tarif
// @Router /tarif [POST]
// @Summary  Create Tarif
// @Description Create Tarif
// @Tags Tarif
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body tarif_service.CreateTarif true "CreateTarifRequestBody"
// @Success 200 {object} http.Response{data=tarif_service.Tarif} "GetTarifBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateTarif(c *gin.Context) {
	var receiveCar order_service.CreateTarif

	err := c.ShouldBindJSON(&receiveCar)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.TarifService().Create(c, &receiveCar)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// GetTarifByID godoc
// @ID get_tarif_by_id
// @Router /tarif/{id} [GET]
// @Summary Get Tarif By ID
// @Description Get Tarif By ID
// @Tags Tarif
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=tarif_service.Tarif} "TarifBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTarifById(c *gin.Context) {

	tarifId := c.Param("id")

	resp, err := h.services.TarifService().GetByID(c, &order_service.TarifPrimaryKey{Id: tarifId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// GetTarifList godoc
// @ID get_tarif_list
// @Router /tarif [GET]
// @Summary Get Tarif s List
// @Description  Get Tarif s List
// @Tags Tarif
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchTarif query string false "searchTarif"
// @Param searchDate query string false "searchDate"
// @Param searchTarifNumber query string false "searchTarifNumber"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=tarif_service.GetListTarifResponse} "GetAllTarifResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetTarifList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.TarifService().GetList(
		context.Background(),
		&order_service.GetListTarifRequest{
			Limit:               int64(limit),
			Offset:              int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// UpdateTarif godoc
// @ID update_tarif
// @Router /tarif/{id} [PUT]
// @Summary Update Tarif
// @Description Update Tarif
// @Tags Tarif
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body tarif_service.UpdateTarif true "UpdateTarifRequestBody"
// @Success 200 {object} http.Response{data=tarif_service.Tarif} "Tarif data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateTarif(c *gin.Context) {
	var tarif order_service.UpdateTarif

	tarif.Id = c.Param("id")

	err := c.ShouldBindJSON(&tarif)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.TarifService().Update(
		c.Request.Context(),
		&tarif,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// Deletetarif godoc
// @ID delete_tarif
// @Router /tarif/{id} [DELETE]
// @Summary Delete Tarif
// @Description Delete Tarif
// @Tags Tarif
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Tarif data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteTarif(c *gin.Context) {

	tarifId := c.Param("id")

	resp, err := h.services.TarifService().Delete(
		c.Request.Context(),
		&order_service.TarifPrimaryKey{Id: tarifId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
