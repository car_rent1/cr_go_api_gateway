package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/car_rent1/cr_go_api_gateway/api/docs"
	"gitlab.com/car_rent1/cr_go_api_gateway/api/handlers"
	"gitlab.com/car_rent1/cr_go_api_gateway/config"
)

func SetUpAPI(r *gin.Engine, h *handlers.Handler, cfg config.Config) {
	// docs.SwaggerInfo.Title = cfg.ServiceName
	// docs.SwaggerInfo.Version = cfg.Version
	// docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(5000))

	r.POST("investor", h.CreateInvestor)
	r.GET("investor/:id", h.GetInvestorById)
	r.GET("investor", h.GetInvestorList)
	r.PUT("investor/:id", h.UpdateInvestor)
	r.DELETE("investor/:id", h.DeleteInvestor)

	r.POST("car", h.CreateCar)
	r.GET("car/:id", h.GetCarById)
	r.GET("car", h.GetCarList)
	r.PUT("car/:id", h.UpdateCar)
	r.DELETE("car/:id", h.DeleteCar)

	r.POST("order", h.CreateOrder)
	r.GET("order/:id", h.GetOrderById)
	r.GET("order", h.GetOrderList)
	r.PUT("order/:id", h.UpdateOrder)
	r.DELETE("order/:id", h.DeleteOrder)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
